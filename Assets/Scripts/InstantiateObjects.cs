﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InstantiateObjects : MonoBehaviour
{
    public GameObject itemToSpread;
    public GameObject hand;
    public int ObjectAmount = 10;
    int tempAmount;



    public float itemXSpread = 10;
    public float itemYSpread = 0;
    public float itemZSpread = 10;



    void spawnSomething()
    {
        Vector3 randomPosition = new Vector3(Random.Range(-itemXSpread, itemXSpread), Random.Range(-itemYSpread, itemYSpread), Random.Range(-itemZSpread, itemZSpread)) + transform.position;
        GameObject clone = Instantiate(itemToSpread, randomPosition, Quaternion.identity);


        string name = "shell" + tempAmount.ToString();

        while (GameObject.Find(name) == true)
        {
            tempAmount += 1;
            name = "ParticleTop" + tempAmount.ToString();
        }
        clone.name = name;
        GameObject.Find(name).transform.parent = GameObject.Find("Emptys").transform;

        
    }

    void Start()
    {
        for (int i=0; i < ObjectAmount; i++)
        {
            spawnSomething();
        }
        
    }
}
