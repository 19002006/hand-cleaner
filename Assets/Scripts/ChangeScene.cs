﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

    public void Start()
    {
        Time.timeScale = 1;
    }

    public void SceneChangeButton()
    {
        SceneManager.LoadScene("GameScene");
        Time.timeScale = 1;

    }

    public void Exit()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void BackSceneButton()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1;



    }

    public void TimeFreeze()
    {
        Time.timeScale = 0;
    }

    public void TimeStart()
    {
        Time.timeScale = 1;
    }
}
