﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEthnicity : MonoBehaviour {

    public GameObject hand;
    public int EthnicitiesAmount;
    public Material[] handEthnicities;
    


	void Start ()
    {
        
        GetComponent<Renderer>().material = handEthnicities[Random.Range(0,(EthnicitiesAmount))];

    }
	

}
