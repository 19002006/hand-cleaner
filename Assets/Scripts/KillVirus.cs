﻿using UnityEngine;

public class KillVirus : MonoBehaviour
{

    public Camera fpsCam;
    int damage = 10;

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 worldMousePosition = fpsCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 100f));

            Vector3 direction = worldMousePosition - fpsCam.transform.position;

            RaycastHit hit;

            if (Physics.Raycast(fpsCam.transform.position, direction, out hit, 100f))
            {

                Debug.DrawLine(fpsCam.transform.position, hit.point, Color.green, 0.5f);
                Target target = hit.transform.GetComponent<Target>();
                if (target != null)
                {
                    target.TakeDamage(damage);
                }
                else
                {
                    Debug.DrawLine(fpsCam.transform.position, worldMousePosition, Color.red, 0.5f);
                }

            }
        }
    }
}




