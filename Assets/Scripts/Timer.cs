﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public float timeLeft = 20f;
    public Image timerUI;
    public Text timerText;
    public GameObject loseScreen;
    public GameObject winScreen;
    float timeFromStart;

    public Gradient gradient;
    public Image fullTimer;
    

	// Use this for initialization
	void Start () {
        winScreen.SetActive(false);
        loseScreen.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        timeFromStart += Time.deltaTime;
        timeLeft -= Time.deltaTime;
        double timeInt = System.Math.Round(timeLeft, 0);

        if (GameObject.Find("Timer") == true)
        {
            timerText.GetComponent<Text>().text = timeInt.ToString();
            timerUI.fillAmount = (timeLeft / 20);
            timerUI.color = gradient.Evaluate(timeLeft/20);



            fullTimer.transform.rotation = Quaternion.Euler(0f,0f,Mathf.Sin(((timeFromStart + 1) *(timeFromStart + 1)/ 2)*3));
            
            
        }

        if (GameObject.Find("hand").transform.childCount == 0)
        {

            Debug.Log("win");
            winScreen.SetActive(true);
            GameObject.Destroy(GameObject.Find("Timer ()"));
        }
        else if (timeLeft <= 0)
        {
            Debug.Log("Lose");

            GameObject.Destroy(GameObject.Find("Timer ()"));
            loseScreen.SetActive(true);
        }

        if (GameObject.Find("hand").transform.childCount > 0)
        {
            winScreen.SetActive(false);
        }


    }
}
