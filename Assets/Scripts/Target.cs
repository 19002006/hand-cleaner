﻿using UnityEngine;
using UnityEngine.UI;

public class Target : MonoBehaviour
{

    public float health = 4f;
    public int particlesEmmitted = 10;
    public ParticleSystem particleLauncher;
    public GameObject particles;
    public AudioClip squelch;
    public AudioClip splash;
    public float audioVolume;
    public Slider InGameSlider;

    public void TakeDamage(float amount)
    {

        audioVolume = ((InGameSlider.value / 80) + 1);

        if (health > 0)
        {
            particleLauncher.Emit(particlesEmmitted);
            gameObject.transform.localScale += new Vector3(-0.05f, -0.05f, -0.05f);
            health -= 1;
            AudioSource.PlayClipAtPoint(splash, new Vector3(9.2f, 28.6f, 0f), Random.Range(audioVolume - 0.1f, audioVolume + 0.1f));
        }
        else
        {
            AudioSource.PlayClipAtPoint(squelch, new Vector3(9.2f, 28.6f, 0f), Random.Range(audioVolume - 0.1f, audioVolume + 0.1f));
            particles.transform.parent = GameObject.Find("Emptys").transform;
            Destroy(gameObject);

        }

    }

}