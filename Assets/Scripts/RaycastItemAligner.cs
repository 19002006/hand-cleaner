﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastItemAligner : MonoBehaviour {

    public float raycastDistance = 100f;
    public GameObject objectToSpawn;
    public int namenum = 1;
    public float overlapTestBoxSize = 1f;
    public LayerMask spawnedObjectLayer;


	// Use this for initialization
	void Start ()
    {
        PositionRaycast();
        PositionRaycastup();

    }




	
	// Update is called once per frame
	void PositionRaycast()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, raycastDistance))
        {
            Quaternion spawnRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

            Vector3 overlapTestBoxScale = new Vector3(overlapTestBoxSize, overlapTestBoxSize, overlapTestBoxSize);
            Collider[] collidersInsideOverlapBox = new Collider[1];
            int numberOfCollidersFound = Physics.OverlapBoxNonAlloc(hit.point, overlapTestBoxScale, collidersInsideOverlapBox, spawnRotation, spawnedObjectLayer);

            if (numberOfCollidersFound == 0)
            {

                ItemSpawn(hit.point, spawnRotation);
            }





        }
    }
    void PositionRaycastup()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.up, out hit, raycastDistance))
        {
            Quaternion spawnRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

            Vector3 overlapTestBoxScale = new Vector3(overlapTestBoxSize, overlapTestBoxSize, overlapTestBoxSize);
            Collider[] collidersInsideOverlapBox = new Collider[1];
            int numberOfCollidersFound = Physics.OverlapBoxNonAlloc(hit.point, overlapTestBoxScale, collidersInsideOverlapBox, spawnRotation, spawnedObjectLayer);

            if (numberOfCollidersFound == 0)
            {
                ItemSpawn(hit.point, spawnRotation);
            }
            




        }
    }


    void ItemSpawn(Vector3 positionToSpawn, Quaternion rotationToSpawn)
    {
        GameObject objclone = Instantiate(objectToSpawn, positionToSpawn, rotationToSpawn);

        string name = "Particle" + namenum.ToString();

        while (GameObject.Find(name) == true)
        {
            namenum += 1;
            name = "Particle" + namenum.ToString();
        }
        objclone.name = name;
        GameObject.Find(name).transform.parent = GameObject.Find("hand").transform;
    }
}
