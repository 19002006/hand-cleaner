﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandRotate : MonoBehaviour {
    int c = 0;
    public int turnspeed = 2;
	
	// Update is called once per frame
	void Update () {
        if (Time.timeScale == 1)
        {
            if (Input.GetKey(KeyCode.D))
            {
                if (c > 0)
                {
                    transform.Rotate(Vector3.right, turnspeed);
                    c -= turnspeed;
                }

            }

            if (Input.GetKey(KeyCode.A))
            {
                if (c < 270)
                {
                    transform.Rotate(Vector3.right, -turnspeed);
                    c += turnspeed;
                }
            }
        }
    }
}
