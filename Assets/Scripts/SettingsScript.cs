﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsScript : MonoBehaviour {

    public AudioMixer audioMixer;
    float setSlider;
    public Slider volumeSlider;
    bool isMute;

    private void Start()
    {
        audioMixer.GetFloat("volume", out setSlider);
        volumeSlider.value = setSlider;
    }


    public void SetVolume(float volume)
    {
        
       

        audioMixer.SetFloat("volume", volume);
    }

    public void Mute()
    {
        isMute = true;
        AudioListener.volume = isMute ? 0 : 1;
    }
    public void Unmute()
    {
        isMute = false;
        AudioListener.volume = isMute ? 0 : 1;
    }
}
